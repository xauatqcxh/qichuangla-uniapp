### 日常签到查询

##### 1.个人的日常签到查询

简要描述:管理员根据学生学号或姓名查询学生日常签到情况

请求url: /daily_query

请求方式: GET

参数: keyword1 string 必选 学号或姓名

```
{
	keyword1:'小明'
}
```

返回数据实例:

```
"data":{
	"Arr":[{
    "date": '2021-8-7', info: '已签到'
},{
    "date": '2021-08-09', info: '已签到'
}],
"code": {
        "msg": "查询成功",
        "status": 200
    }
}
```

##### 2.班级日常签到查询

简要描述:管理员输入班级名 查询班级各个成员的签到情况

请求url : /daily_class_query

请求方式:GET

参数 keyword2 必选 String 班级名称

```
{
	keyword2:'软件工程1501'
}
```

返回数据实例:

```
data:{
	"Class":[
	        {ur_user_name:'小明',ur_number:1,info:'已签到'},
			{ur_user_name:'小红',ur_number:2,info:'未签到'},
			{ur_user_name:'小黑',ur_number:3,info:'已签到'},
			{ur_user_name:'小白',ur_number:4,info:'未签到'}],
	"code": {
        "msg": "查询成功",
        "status": 200
    }
}
```

其中 ur_user_name为学生姓名，ur_number为学生学号，info为学生当天的签到情况



### 活动签到查询

##### 1.当前举行的所有活动查询

简要描述:查询当前仍举行的活动及详细信息

请求url : /get_activity

请求方式 ：GET

参数：无

返回数据格式：

```
data:{
			Activity:[{
					aid:1,
					ac_name:'奔跑吧',
					ac_desac_des:'《奔跑吧兄弟》是浙江卫视引进韩国SBS电视台综艺节目《Running Man》推出的大型户外竞技真人秀节目。'
				},{
					aid:2,
					ac_name:'极限挑战',
					ac_desac_des:'《极限挑战》是东方卫视制作的一档大型励志体验真人秀节目。每期成员进行开放式的户外挑战运动。'
				},{
					aid:3,
					ac_name:'天天向上',
					ac_des:'《天天向上》由湖南经视《越策越开心》原班人马打造。兼有“策”的神韵, 更有发扬中华美德的社会责任。'
					
				}
				],
			"code": {
        				"msg": "查询成功",
        				"status": 200
    				}
}
```

其中 aid为活动唯一标识id ，ac_name为活动标题，ac_des为活动的内容。





##### 2.活动搜索

简要描述:管理员通过活动名查询活动。

请求url: /query_activity

请求方式:GET

参数: keyword 必选 String 活动名称

```
{
	keyword:'天天向上'
}
```

返回的数据格式：

```
data:{
			Activity:[{
					aid:1,
					ac_name:'奔跑吧',
					ac_desac_des:'《奔跑吧兄弟》是浙江卫视引进韩国SBS电视台综艺节目《Running Man》推出的大型户外竞技真人秀节目。'
				}],
			"code": {
        				"msg": "查询成功",
        				"status": 200
    				}
}
```









##### 3.活动查询

简要描述:管理员查询活动下的小组学生的签到情况。

请求url: /query_activity

请求方式:GET

参数:ac_name 必选 String 活动名称

```
{
	keyword:'天天向上'
}
```

返回数据格式:

```
data:
		{
			itemList: [{
				gp_name: "中国队",
				Gmember:[
							{ur_user_name:'小明',ur_number:1,info:'已签到'},
							{ur_user_name:'小红',ur_number:2,info:'未签到'},
							{ur_user_name:'小黑',ur_number:3,info:'已签到'},
							{ur_user_name:'小白',ur_number:4,info:'未签到'}
						]			
						},{
							gp_name: "日本队",
							Gmember:[
							{ur_user_name:'小绿',ur_number:6,info:'已签到'},
							{ur_user_name:'小蓝',ur_number:7,info:'未签到'},
							{ur_user_name:'小紫',ur_number:8,info:'已签到'},
							{ur_user_name:'小花',ur_number:9,info:'未签到'}]
						  },
			"code": {
        				"msg": "查询成功",
        				"status": 200
    				}
		}
```

  gp_name 为小组名称,

 Gmember 为小组成员列表,

  ur_user_name为小组成员姓名 

  ur_number为小组成员学号,info为小组成员活动签到情况



