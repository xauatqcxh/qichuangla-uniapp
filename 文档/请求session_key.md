####                                                            请求session_key

简要描述：前端请求session_key

请求url:  authsys-user/wxlogin

请求方式: GET	

参数：code 必选 string类型 临时登录凭证。

请求的数据格式: 

```
{

	code:"001tqPGa1IrmzB0o4IFa1Z4KcH0tqPGh"

}
```

返回示例：

```
data:{
	session_key:"",
}
```



![返回](C:\Users\yume\Desktop\返回.jpg)

