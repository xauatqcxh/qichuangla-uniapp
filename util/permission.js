
export default function checkPermission(value) {
	// console.log(value)
  if (value && value instanceof Array && value.length > 0) {
    // 获取当前登录用户，拥有的角色列表
    const roles = uni.getStorageSync("role")
    const permissionRoles = value
    // 判断value和roles两个集合，是否有交集
    return permissionRoles.includes(roles)
  } else {
    console.error(`need roles! Like v-permission="['admin','editor']"`)
    return false
  }
}
