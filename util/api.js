// https://www.bilibili.com/video/BV1BJ411W7pX?p=32  http请求方式

const BASE_URL = 'https://wangzhenbang.com'
var headerData = {
	header:{
		
		// 'Cookie':''
	}
}
import Vue from 'vue'
Vue.prototype.$util = headerData;
export const myRequest = (options)=>{
	return new Promise((resolve,reject)=>{
		uni.request({
			url:BASE_URL+options.url,
			method:options.method || 'GET',
			data:options.data || {},
			header:{
				'X-Token':uni.getStorageSync('token')
			},
			success:(res)=>{
			
				
				resolve(res)
			},
			fail:(err)=>{
				uni.showToast({
					title:'请求接口失败'
				})
				reject(err)
			}
		})
	})
}