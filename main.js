import Vue from 'vue'
import App from './App'
import { myRequest } from './util/api.js'
import uView from "uview-ui";
import './router' // 引入路由
Vue.use(uView);
Vue.prototype.$myRequest = myRequest

Vue.config.productionTip = false

App.mpType = 'app'

const app = new Vue({
    ...App
})
app.$mount()
